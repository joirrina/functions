"use strict";

function getNumberString(str){
  if (typeof(str) != 'string' || isNaN(str)) {
    return null;
  }
  return Number(str);
}

const getSum = (str1, str2) => {
  let num1 = getNumberString(str1);
  let num2 = getNumberString(str2);
  return (num1 == null || num2 == null) ? false : (num1 + num2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = [];
  posts = listOfPosts.filter(x => x.author == authorName);
  let countComments = 0;
  for (let item of listOfPosts) {
    if (item.hasOwnProperty('comments')) {
      let comments = [];
      comments = item.comments.filter(x => x.author == authorName);
      countComments += comments.length; 
    }
  }    
  return `Post:${posts.length},comments:${countComments}`;
};

function countChange(Vasya, change) {

  if (change == 0 ) {
    return change;
  }

  if (Vasya['100'] > 0 && change >= 100) {
    change -= 100;
    Vasya['100'] -= 1;
    if (change >= 0) {
      return countChange(Vasya, change);  
    }
  } 
  else if(Vasya['50'] > 0 && change >= 50) {
    change -= 50;
    Vasya['50'] -= 1;
    if (change >= 0) {
      return countChange(Vasya, change);  
    } 
  }
  else if(Vasya['25'] > 0 && change >= 25) {
    change -= 25;
    Vasya['25'] -= 1;
    if (change >= 0) {
      return countChange(Vasya, change);  
    } 
  } 
  else {
    return change;
  }
}

const tickets=(people)=> {
  let answer = 'YES';
  let Vasya = {'25': 0, '50': 0, '100': 0};
  for (let bill of people) {
    if (!Vasya.hasOwnProperty(bill.toString())) {
      answer = 'NO';
      break; 
    }  
    var change = bill - 25;
    change = countChange(Vasya, change);
    if (change == 0) {      
      Vasya[bill.toString()] += 1;         
    }
    else{
      answer = 'NO';
      break;  
    }
  }
  return answer;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
